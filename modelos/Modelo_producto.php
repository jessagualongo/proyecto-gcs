<?php
 class Modelo_producto extends Conexion {
	public $cod_pro;
	public $nombre_pro;
	public $precio_compra;
	public $precio_venta;
	public $categoria;
	public $cantidad;
	public $estado;
 	public function __construct(){
        parent::__construct();
    }
    public function Guardar( Modelo_producto $param){
            try{
                $attr="(cod_pro, nombre_pro,precio_venta, precio_compra,categoria,cantidad,estado)";
                $tabla="producto";
                $datos="?, ?, ?, ?, ?, ?,?";
                $this->estado="A";
                $parametros=array($param->cod_pro,$param->nombre_pro,$param->precio_compra,$param->precio_venta,$param->categoria,$param->cantidad,$this->estado);
                $insert=$this->pd->registros($attr,$tabla,$datos,$parametros);
                return $insert;
            }
            catch (PDOException $e ){
                return $e->getMessage();
            }
        }
        public function Actualizar( Modelo_producto $param){
            try{
                $tabla="producto";
                $condicion="cod_pro='".$param->cod_pro."'";
                $datos="cod_pro='".$param->cod_pro."',
                nombre_pro='".$param->nombre_pro."',
                precio_compra=".$param->precio_compra.",
                precio_venta=".$param->precio_venta."
                ,categoria='".$param->categoria."',
                cantidad=".$param->cantidad.",
                estado='".$param->estado."'";
                $actualiza=$this->pd->actualizar($condicion,$tabla,$datos);
                return $actualiza;
            }
            catch (PDOException $e ){
                return $e->getMessage();
            }
        }
    public function listar_a(){
        	$attr="*";
    		$table="producto";
    		$where="estado='A'";
    		$consulta=$this->pd->consultas($attr,$table,$where);
    		return $consulta;
        }
     public function listar_i(){
        	$attr="*";
    		$table="producto";
    		$where="estado='I'";
    		$consulta=$this->pd->consultas($attr,$table,$where);
    		return $consulta;
        }
     public function editar($cod){
            $tabla="producto";
            $id="cod_pro='".$cod."'";
            $consulta=$this->pd->get_id_table($tabla,$id); //echo json_encode($consulta);
            return json_encode($consulta);
        }
     public function eliminar($id){
            try{
                $tabla="producto";
                $condicion="cod_pro='".$id."'";
                $datos="estado='I'";
                $actualiza=$this->pd->actualizar($condicion,$tabla,$datos);
                return $actualiza;
            }
            catch (PDOException $e ){
                return $e->getMessage();
            }
        }

 }	  
?>