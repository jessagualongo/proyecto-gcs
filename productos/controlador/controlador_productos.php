<?php 
	class controlador_productos
	{
	  private $model;
	  public function __construct()
	  {
	    $this->model = new Modelo_producto();
	  }
	  public function index(){
	  	include"vistas/cabecera.php";
	  	include"vistas/menu.php";
	  	include"vistas/inicio.php";
	  	include"vistas/pie.php";
	  }
	  public function inicio(){
	  	include"vistas/cabecera.php";
	  	include"vistas/menu2.php";
	  	include"vistas/inicio_user.php";
	  	include"vistas/pie.php";
	  }

	  public function registro(){
	  	include"vistas/cabecera.php";
	  	include"vistas/menu2.php";
	  	include"vistas/registro.php";
	  	include"vistas/pie.php";
	  }
	  public function guardar(){
	  	$this->model->cod_pro = $_POST["cod_pro"];
	  	$this->model->nombre_pro = $_POST["nombre_pro"];
	  	$this->model->precio_compra = $_POST["precio_compra"];
	  	$this->model->precio_venta = $_POST["precio_venta"];
	  	$this->model->categoria = $_POST["categoria"];
	  	$this->model->cantidad = $_POST["cantidad"];
	  	$ac=$this->model->Guardar($this->model);
	  	if($ac=="true"){
		    	echo "ingresado";
		    	header("location:?c=productos&a=registro&e=1");
		    }
		    else{
		    	echo "Fallo al registrar";
		    	header("location:?c=productos&a=registro&e=0");
		    }
	  }
	  public function actualizar(){
	  	$this->model->cod_pro = $_POST["cod_pro"];
	  	$this->model->nombre_pro = $_POST["nombre_pro"];
	  	$this->model->precio_compra = $_POST["precio_compra"];
	  	$this->model->precio_venta = $_POST["precio_venta"];
	  	$this->model->categoria = $_POST["categoria"];
	  	$this->model->cantidad = $_POST["cantidad"];
	  	$this->model->estado = $_POST["estado"];
	  	$ac=$this->model->Actualizar($this->model);
	  	if($ac=="true"){
		    	header("location:?c=productos&a=visualizar");
		    }
		    else{
		    	header("location:?c=productos&a=visualizar");
		    }

	  }
	  public function visualizar(){
			$row=$this->model->listar_a();
			$row2=$this->model->listar_i();
			include"vistas/cabecera.php";
	  		include"vistas/menu2.php";
			include 'vistas/lista_p.php';
    		include"vistas/pie.php";
		}
	  public function editar(){
	  		@$cod=$_GET["id"];
			$row=$this->model->editar($cod);
			$row=json_decode($row);
			include"vistas/cabecera.php";
	  		include"vistas/menu2.php";
	  		include"vistas/registro.php";
    		include"vistas/pie.php";
		}
		public function eliminar(){
     	$id=$_GET["id"];
     	$ac=$this->model->eliminar($id);
     	if($ac=="true"){
		    	header("location:?c=productos&a=visualizar");
		    }
		    else{
		    	header("location:?c=productos&a=visualizar");
		    }
     }
	}
?>