 <?php
    if(isset($_GET["e"])){
        $e=$_GET["e"];
        if($e==1){
            $mensaje='<div class="mensaje">Poducto registrado corectamente</div>';
        }
        else{
            $mensaje='<div class="mensaje">Error al registrar</div>';
        }
    }
    else{
        $mensaje="";
    }
    if(isset($_GET["ac"])){
        $ac=$_GET["ac"];
        if($ac==1){
            $mensaje2='<div class="mensaje">Poducto actualizado corectamente</div>';
        }
        else{
            $mensaje2='<div class="mensaje">Error al actualizar</div>';
        }
    }
    else{
        $mensaje2="";
    }
    
    ?>
<div class="cuerpo">
        <!-- inicio de modal de registro-->
        <?php echo @$mensaje;?>
        <?php echo @$mensaje2;?>
        <?php 
        echo '<script type="text/javascript">
            setTimeout(function(){
                document.querySelector(".mensaje").remove();
            },4000);
        </script>';
        ?>
        <div class="c_modal c_registro">
            <header class="cabecera_modal">
                <a href="?c=productos&a=visualizar" class="cerrar_modal">X</a>
                <h1><?php echo @$hotel->cod_pro!=null ?'Acualizar' : 'Registrar';?> producto</h1>
                <?php if (@$_GET["id"]) {
                    $a='actualizar';
                    
                }
                else{
                    $a='guardar';
                }
                ?>
            </header>
                <div class="c_form">
                    <form action="?c=productos&a=<?php echo @$a;?>" method="POST">
                        <h4>Codigo de producto</h4>
                        <input class="input" type="text" name="cod_pro" placeholder="Codigo de producto" value="<?php echo @$row->cod_pro;?>"/>
                        <h4>Nombre</h4>
                        <input class="input" type="text" name="nombre_pro" placeholder="Nombre" value="<?php echo @$row->nombre_pro;?>"/>
                        <h4>Precio de compra</h4>
                        <input class="input" type="number" step="0.01"  name="precio_compra" placeholder="Precio de Compra" value="<?php echo @$row->precio_compra;?>"/>
                        <h4>Precio de venta</h4>
                        <input class="input" type="number" step="0.01"  name="precio_venta" placeholder="Precio de venta" value="<?php echo @$row->precio_venta;?>"/>
                        <h4>Categoria</h4>
                        <select class="input" name="categoria">
                            <option value="<?php echo @$row->categoria;?>"><?php echo @$row->categoria;?></option> 
                            <option value="Carnicos">Carnicos</option> 
                            <option value="Bebidas">Bebidas</option> 
                            <option value="Limpieza">Limpieza</option>
                        </select>
                        <h4>Cantidad</h4>
                        <input class="input" type="text" name="cantidad" placeholder="Cantidad" value="<?php echo @$row->cantidad;?>"/>
                        <?php echo @$hotel->cod_pro!=null ?'' : '<h4>Estado</h4>
                        <input class="input" type="text" name="estado" value="'.@$row->estado.'"/>';?>
                        <input class="bt_registro" type="submit" name="registrar" value="Guardar">
                    </form>
                </div>
        </div>
    </div>