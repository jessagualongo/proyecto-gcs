CREATE DATABASE productos WITH OWNER = postgres ENCODING = UTF8;
\c productos
CREATE TABLE producto(
		cod_pro varchar(5) primary key, 
		nombre_pro varchar(20), 
		precio_compra float, 
		precio_venta float, 
		categoria varchar(10),
		cantidad int,
		estado varchar(10)
		);
$datos=array('cod_pro'=>$param->cod_pro,'nombre_pro'=>$param->nombre_pro,'precio_compra'=>$param->precio_compra,'precio_venta'=>$param->precio_venta,'categoria='=>$param->categoria,'cantidad'=>$param->cantidad,'estado'=>$param->estado);