	<!-- inicio de cuerpo-->
<div class="cuerpo">
	<h1>Listado de productos</h1>
	<div class="c_tabla">
		<h1>Productos activos</h1>
		<table class="tabla">
			<th>Codigo</th>
			<th>Nombre</th>
			<th>Precio de venta</th>
			<th>Precio de compra</th>
			<th>Categoria</th>
			<th>Cantidad</th>
			<th>Opcion</th>
			<tbody>
			<?php 
			foreach ($row as $key) {
				echo '
				<tr>
					<td>'.$key["cod_pro"].'</td>
					<td>'.$key["nombre_pro"].'</td>
					<td>'.$key["precio_compra"].'</td>
					<td>'.$key["precio_venta"].'</td>
					<td>'.$key["categoria"].'</td>
					<td>'.$key["cantidad"].'</td>
					<td><a href="?c=productos&a=editar&id='.$key["cod_pro"].'">Editar</a> </td>
					<td><a href="?c=productos&a=eliminar&id='.$key["cod_pro"].'">Eliminar</a> </td>
				</tr>
				';		
			?>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
	<div class="c_tabla">
		<h1>Productos Inactivos</h1>
		<table class="tabla">
			<th>Codigo</th>
			<th>Nombre</th>
			<th>Precio de venta</th>
			<th>Precio de compra</th>
			<th>Categoria</th>
			<th>Cantidad</th>
			<th>Opcion</th>
			<tbody>
			<?php 
			foreach ($row2 as $key2) {
				echo '
				<tr>
					<td>'.$key2["cod_pro"].'</td>
					<td>'.$key2["nombre_pro"].'</td>
					<td>'.$key2["precio_compra"].'</td>
					<td>'.$key2["precio_venta"].'</td>
					<td>'.$key2["categoria"].'</td>
					<td>'.$key2["cantidad"].'</td>
					<td><a href="?c=productos&a=editar&id='.$key2["cod_pro"].'">Editar</a> </td>
					<td><a href="?c=productos&a=eliminar&id='.$key2["cod_pro"].'">Eliminar</a> </td>
				</tr>
				';		
			?>
			<?php
			}
			?>
			</tbody>
		</table>
	</div>
</div>
	<!-- fin de cuerpo-->